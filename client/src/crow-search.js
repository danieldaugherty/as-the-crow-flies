import React, { Component } from 'react';
import axios from 'axios';
import './crow-search.css';

class CrowSearch extends Component {
  constructor(props) {
    super(props);

    this.debounceCounter = 0;

    this.state = {};
    this.state.value = '';
  }

  onChange = event => {
    const self = this;
    const value = event.target.value;

    this.setState({value});

    this.props.updatePlace(undefined); // unset place ID on parent

    if (value.length < 2) {
      this.setState({results: undefined});
      return; // don't get suggestions if nothing to fetch
    }

    ++this.debounceCounter;
    setTimeout(() => {
      --self.debounceCounter;
      if (self.debounceCounter === 0) {
        this.getSuggestions(value);
      }
    }, 200); // get suggestions after 200ms debounce
  }

  onClick = (suggestion) => {
    const placeID = suggestion.place_id;

    this.setState({value: suggestion.name, results: undefined}); // hide suggestions, update input value
    this.props.updatePlace(placeID); // update place ID on parent
  }

  getSuggestions = query => {
    axios.get('http://localhost:2001/airports', {params: {q: query}})
      .then(res => {
        this.setState({results: res.data.results});
      })
      .catch(console.error);
  }

  render() {
    const self = this;
    function showList() {
      if (!self.state.results) {
        return;
      } else if (!self.state.results.length) {
        // Searching, no results found
        return <tr className="search-result"><td>No results found.</td></tr>
      }
      // Either results found or not searching
      return self.state.results.map(result => (
        <tr className="search-result" key={result.id} onClick={() => self.onClick(result)}>
          <td>{result.name}</td>
        </tr>
      ))
    }
    return (
      <div className="crow-search">
        <input type="text" 
               value={this.state.value}
               className="form-control"
               placeholder="Search airports..."
               onChange={this.onChange}/>
        <table className="search-result-list">
          <tbody>
            {
              showList()
            }
          </tbody>
        </table>
      </div>
    );
  }
}

export default CrowSearch;
