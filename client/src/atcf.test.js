import React from 'react';
import ReactDOM from 'react-dom';
import ATCF from './atcf';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ATCF />, div);
  ReactDOM.unmountComponentAtNode(div);
});
