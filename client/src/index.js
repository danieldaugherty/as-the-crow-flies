import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ATCF from './atcf';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<ATCF />, document.getElementById('root'));
registerServiceWorker();
