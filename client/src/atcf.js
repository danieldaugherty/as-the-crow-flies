import React, { Component } from 'react';
import CrowSearch from './crow-search.js';
import axios from 'axios';
import './atcf.css';

class ATCF extends Component {
  constructor() {
    super();
    this.state = {distance: 0};
  }

  calculateDistance = (from, dest) => {
    const self = this;

    axios.get('http://localhost:2001/distance', {
      params: {
        from: from,
        dest: dest
      }
    })
      .then(res => {
        const meters = res.data.rows[0].elements[0].distance.value;
        // 0.000539957 meters/nautical mile
        const distance = Math.round(meters * 539957 / 10000000) / 100; // round to the tenths
        self.setState({distance});
      })
      .catch(console.error);
  }

  render() {
    const self = this;
    const updateFrom = placeID => {
      self.setState({from: placeID});
      if (placeID) {
        if (self.state.dest) { //  if both from & dest are set
          self.calculateDistance(placeID, self.state.dest);
        }
        // if only one is set, do nothing
      } else {
        self.setState({distance: 0}); // reset distance when place ID is reset
      }
    }
    const updateDest = placeID => {
      self.setState({dest: placeID});
      if (placeID) {
        if (self.state.from) { //  if both from & dest are set
          self.calculateDistance(self.state.from, placeID);
        }
        // if only one is set, do nothing
      } else {
        self.setState({distance: 0}); // reset distance when place ID is reset
      }
    }
    return (
      <div className="atcf">
        <header className="atcf-header">
          <h1 className="atcf-title">
            As The<br/>
            <img src="./crow.png" className="atcf-logo" alt="logo" /> Flies
          </h1>
        </header>
        <div className="container">
          <div className="atcf-intro">
            Specify two airports, and ATCF will tell you the distance between them in nautical miles.
          </div>
          <div className="actf-distance">Calculated Distance: {this.state.distance} Nautical Miles</div>
          <div className="row">
            <div className="actf-from col-6">
              <CrowSearch updatePlace={updateFrom}/>
            </div>
            <div className="atcf-dest col-6">
              <CrowSearch updatePlace={updateDest}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ATCF;
