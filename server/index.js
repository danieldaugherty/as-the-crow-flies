'use strict';

const express = require('express');
const app = express();
const http = require('http');
const request = require('request-promise');
const config = require('./config');


app.all('/*', (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
});

app.get('/airports', (req, res) => {
  if (!req.query.q) {
    res.json([]);
  }

  request({
    method: 'GET',
    url: 'https://maps.googleapis.com/maps/api/place/textsearch/json?type=airport&key=' + config.MAPS_API_KEY + '&query=' + req.query.q.replace(' ', '+'),
    json: true,
  })
    .then(body => {
      res.json(body);
    })
    .catch(err => {
      console.log('failed!', (err.message ? err.message + '\n' : '') + (err.stack || ''));
      res.status(500).send('An internal error occurred.');
    });
});

app.get('/distance', (req, res) => {
  if (!req.query.from || !req.query.dest) {
    res.json([]);
  }

  request({
    method: 'GET',
    url: 'https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyCe-x1hC-MzqEHh645sb4JGBZhzHpKxafg&origins=place_id:' + req.query.from + '&destinations=place_id:' + req.query.dest,
    json: true,
  })
    .then(body => {
      res.json(body);
    })
    .catch(err => {
      console.log('failed!', (err.message ? err.message + '\n' : '') + (err.stack || ''));
      res.status(500).send('An internal error occurred.');
    });
});

http.createServer(app).listen(2001);

console.log('You are now free to roam about the Google Maps API');